#include <iostream> 
#include <cmath>
#include <cstdlib>
#include <iomanip>
#include <ctime>
using namespace std;

const int Size = 30, ControlValue1 = -21, ControlValue2 = 45, Size2 = Size - 1;
void CreateArray(int Array[Size]); 
void OutputArray(int Array[Size], int _Size);
void OutputSymbol(int Symbol);
void GetMaxEvenEl(int Array[Size]);
void GetMinEl(int Array[Size]);
void ReplaceTheZeroEl(int Array[Size]);
void DeleteOfEl(int _Array[Size], int _NewArray[Size2], int _Size);

int main()
{
	int Array_A[Size] = { 0 };
	int NewArray_A[Size2] = { 0 };
	cout << "Array A:" << endl;
	CreateArray(Array_A);
	OutputArray(Array_A, Size);
	GetMaxEvenEl(Array_A);
	GetMinEl(Array_A);
	ReplaceTheZeroEl(Array_A);
	DeleteOfEl(Array_A, NewArray_A, Size);
	system("pause");
	return 0;
}

void CreateArray(int Array[Size])
{
	srand(time(0));
	for (int i = 0; i < Size; i++)
	{
		Array[i] = ControlValue1 + rand() % ControlValue2;
	}
}

void OutputArray(int Array[Size], int _Size)
{
	int Counter = 0;
	for (int i = 0; i < _Size; i++)
	{
		cout << Array[i] << " ";
		Counter++;
		if (Counter == 10)
		{
			cout << endl;
			Counter = 0;
		}
	}
	cout << endl;
}

void OutputSymbol(int Symbol)
{
	cout << Symbol << endl;
}

void GetMaxEvenEl(int Array[Size])
{
	int MaxEl;
	bool Flag = true;
	for (int i = 0; i < Size; i++)
	{
		if (((abs(Array[i])) % 2 == 0) && (Flag))
		{
			MaxEl = Array[i];
			Flag = false;
		}
	}
	for (int i = 0; i < Size; i++)
	{
		if (((abs(Array[i])) % 2 == 0) && (MaxEl < Array[i]))
		{
			MaxEl = Array[i];
		}
	}
	cout << "The maximal even element is: ";
	OutputSymbol(MaxEl);
}
void GetMinEl(int Array[Size])
{
	int MinEl = Array[0];
	int A;
	cout << "Enter  A: ";
	cin >> A;
	for (int i = 0; i < Size; i++)
	{
		if ((Array[i] % A == 0) && (MinEl > Array[i]))
			MinEl = Array[i];
	}
	cout << "The minimal element is a multiple of A: " << MinEl<<endl;
}
int GetSumEvenEl(int Array[Size])

{
	int Sum = 0;
	for (int i = 0; i < Size; i = i + 2)
	{
		Sum += Array[i];
	}
	return Sum;
}

void ReplaceTheZeroEl(int Array[Size])

{
	int Sum;
	for (int i = 0; i < Size; i++)
	{
		if (Array[i] == 0)
		{
			Sum = GetSumEvenEl(Array);
			cout << "The sum  of elements with even indices =: ";
			OutputSymbol(Sum);
			Array[i] = Sum;
		}
	}
	cout << "Array B:" << endl;
	OutputArray(Array, Size);
}

void DeleteOfEl(int _Array[Size], int _NewArray[Size2], int _Size)
{
	int NumbEl;
	cout << endl;
	cout << "Enter the elements number for delete" << endl;
	cin >> NumbEl;
	if (NumbEl <= 30)
	{
		int j = 0;
		for (int i = 0; i < _Size; i++)
		{
			if (i != (NumbEl - 1))
			{
				_NewArray[j] = _Array[i];
				j += 1;
			}
		}
		cout << "Array after deleting of element:" << endl;
		OutputArray(_NewArray, Size2);
	}
	else
		cout << "Error, there is not element with this number!" << endl;
}